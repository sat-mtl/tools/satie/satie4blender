# SATIE4Blender changelog

## [0.1.1] - 2021-05-17
### Features
- Added License file
- A short illustrated *Getting started* guide

## [0.1.0] - 2021-03-21
This is a first, experimental release!
### Features
- Preferences window accessible from the Tool Panel
- (Des)Activate OSC communication via the Tool Panel
- Configurable logging levels from Preferences
- OSC connection configuration from Preferences
- Object Properties panel
  - List SATIE types
  - Instantiate/destroy SATIE objects
  - Display properties of currently selected SATIE instance
- Selectable Listener object
- Saving of SATIE state with .blend file

### Known issues
- When opening a saved files, need to select the objects with SATIE instances attached for them to get activated (send messages to SATIE server). Selecting entire scene (`a a`) does the job
- When creating a new SATIE instance, need to specify the Listener object explicitely, i.e. whatever is shown in the popup will not take effect until confirmed by a click.
