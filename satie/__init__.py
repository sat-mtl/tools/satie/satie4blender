__copyright__ = """
This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public
License as published by the Free Software Foundation; either version 3 of the license, or (at your option) any later
version (http://www.gnu.org/licenses/).
"""
import logging.config
from threading import Timer

import bpy

from bpy.props import PointerProperty
from pysatie.satie import Satie

from .Properties import SatieObjectProperties
from . import Control
# from .satie import Satie

from . import auto_load         # source: https://gist.github.com/JacquesLucke/11fecc6ea86ef36ea72f76ca547e795b

__license__ = "GPLv3"

bl_info = {
    "name": "Satie OSC",
    "author": "Michal Seta",
    "version": (0, 1, 1),
    "blender": (2, 90, 0),
    "location": "View 3D > Tool Shelf > SATIE panel",
    "warning": "Early stages of development",
    "wiki_url": "",
    "description": "Author SATIE audio scenes with Blender",
    "category": "User",
}

LOGGING_CONFIG = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'standard': {
            'format': '%(asctime)s [%(levelname)s] %(name)s: %(message)s'
        },
    },
    'handlers': {
        'default': {
            'level': 'DEBUG',
            'formatter': 'standard',
            'class': 'logging.StreamHandler',
            'stream': 'ext://sys.stdout',  # Default is stderr
        },
    },
    'loggers': {
        'satie': {
            'handlers': ['default'],
            'level': 'DEBUG',
            'propagate': False
        },
        'pysatie.satie': {
            'handlers': ['default'],
            'level': 'INFO',
            'propagate': False
        },

        'pysatie.oscserver': {
            'handlers': ['default'],
            'level': 'INFO',
            'propagate': False
        },
    }
}

auto_load.init()


def register() -> None:
    auto_load.register()
    bpy.types.Object.satie = PointerProperty(type=SatieObjectProperties)

    log = logging.getLogger(__name__)

    # Put satie instance in bpy in order to share it with other addons
    bpy.satie = Satie()
    bpy.satie_instances = []
    bpy.satie_animated_instances= []
    Control.satie = bpy.satie
    bpy.app.handlers.depsgraph_update_pre.append(Control.satie_instance_cb)
    bpy.app.handlers.frame_change_pre.append(Control.satie_instance_cb)
    bpy.app.handlers.load_post.append(Control.load_handler)
    preferences = bpy.context.preferences.addons[__package__.split('.')[0]].preferences
    bpy.satie.destination_port = preferences.osc_destination_port
    bpy.satie.destination_host = preferences.osc_destination
    bpy.satie.server_port = preferences.osc_server_port
    LOGGING_CONFIG['loggers']['satie']['level'] = preferences.addon_logging
    LOGGING_CONFIG['loggers']['pysatie.satie']['level'] = preferences.pysatie_satie_logging
    LOGGING_CONFIG['loggers']['pysatie.oscserver']['level'] = preferences.pysatie_osc_logging
    logging.config.dictConfig(LOGGING_CONFIG)
    preferences.logging_config = LOGGING_CONFIG
    active = preferences.active
    log.debug(
        f"preferences: destination_port:{bpy.satie.destination_port}\
        destination_host:{bpy.satie.destination_host}\
        server_port: {bpy.satie.server_port}"
    )
    if active:
        bpy.satie.initialize()
        bpy.satie.query_audiosources()


def unregister() -> None:
    bpy.satie.disconnect()
    del bpy.satie
    del bpy.types.Object.satie
    bpy.app.handlers.depsgraph_update_pre.remove(Control.satie_instance_cb)
    auto_load.unregister()


if __name__ == "__main__":
    register()
