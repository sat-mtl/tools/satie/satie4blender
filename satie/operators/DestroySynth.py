import bpy
from bpy.types import Operator


class DestroySynth(Operator):
    bl_idname = "satie.destroy"
    bl_label = "Destroy Synth"
    bl_description = "Destroy a synth instance"
    bl_options = {'INTERNAL'}

    def execute(self, context):
        satie = bpy.satie
        obj = context.object
        satie.delete_node(obj.name)
        bpy.satie_instances.remove(obj)
        context.object.satie.enabled = False
        return {"FINISHED"}
