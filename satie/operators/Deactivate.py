import bpy
from bpy.types import Operator


class Deactivate(Operator):
    bl_idname = "satie.deactivate"
    bl_label = "Deactivate"
    bl_description = "Deactivate SATIE (save user preferences to disable auto-connect)"

    def execute(self, context):
        bpy.context.preferences.addons[__package__.split('.')[0]].preferences.active = False
        return {"FINISHED"}
