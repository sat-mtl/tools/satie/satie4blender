import logging

import bpy
from bpy.types import Panel


logger = logging.getLogger(__name__)

class ToolsPanel(Panel):
    """
    Tool panel UI for Satie
    """
    bl_idname = "SATIE_PT_tool_edit"
    bl_label = "SATIE Tool"
    bl_context = "objectmode"
    bl_category = "SATIE View"
    bl_space_type = "VIEW_3D"
    bl_region_type = "UI"

    def draw_header(self, context):
        layout = self.layout
        layout.label(text='', icon='SPEAKER')

    def draw(self, context):
        layout = self.layout
        col = layout.column()

        col.operator("satie.configure", icon='PREFERENCES')

        col.separator()

        if bpy.context.preferences.addons[__package__].preferences.active:
            col.operator("satie.deactivate", icon='SPEAKER')
            layout.label(text="SATIE Connection Active")
        else:
            col.operator("satie.activate", icon='CANCEL')
            layout.label(text="SATIE Connection Inactive")


class VIEW3D_PT_SatiePanel(bpy.types.Panel):
    """
    Blender Object properties panel for Satie
    """
    bl_idname = "SATIE_PT_properties_panel"
    bl_space_type = "PROPERTIES"
    bl_region_type = "WINDOW"
    bl_context = "object"
    bl_label = "SATIE properties"

    @classmethod
    def poll(cls, context):
        return context.active_object is not None

    def draw(self, context):
        satie = bpy.satie
        obj = context.object
        props = obj.satie

        synth = satie.get_node_by_name(obj.name)

        row = self.layout.row()
        col = row.column(align=True)

        col.prop(props, "ui_plugin_family")
        col.prop(props, "ui_synth")
        col.prop(props, "group")
        row.active = synth is None

        row = self.layout.row()
        col = row.column(align=True)

        if synth is None:
            col.operator("satie.create", icon='SPEAKER')
            col.label(text="Create an instance to edit its properties")
        else:
            col.operator("satie.destroy", icon='CANCEL')

            # General
            row = col.row(align=True)
            row.prop(props, "active")
            row.prop(props, "debug")

            # Properties
            if synth is not None:
                col.prop(props, "ui_listener")
                synth_props = synth.plugin.properties
                for property in synth_props:
                    if obj.SynthParameters:
                        try:
                            col.prop(obj.SynthParameters, property.name)
                            logger.debug(f"Added new property {property.name}")
                        except AttributeError as e:
                            logger.error(f"Cannot add property {property.name} because: {e}")
